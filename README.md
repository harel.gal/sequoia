# Sequoia
## Resources and functions representational protocol middleware.


1. [Description](#Description)
2. [Building-blocks](#Building-blocks)
3. [Uri structure](#Resource-URI)
4. walk-through
5. Actors
6. Stores
7. MS-Sql implementation
8. Postgres implementation
9. Static implementation
10. Utils implementation
11. Post-Processor
12. Tracers
13. message instructions (Demo message)
14. NodeJs implementation
15. Client (consumer/dispatcher) integration

### Description ###
##### *Sequoia* is a uri resource representation and translation service, a uri can represent: #####
1. static value 
2. Js lib or function (random, guid, datetime etc.)
3. database query, stored proc, function (mssql, postgres or any RDBMS).
4. remote service (not implemented yet)

### Building-blocks ###
##### sequoia building blocks are its **actors** and **stores**

an actor is a facade/provider which is responsible for connecting and resolving the values for a specific vendor that we want to query and free its resources once done.
### Resource URI ###
##### The basic uri structure is microservices and transport protocols ready.

> uri structure: [service name].[method name]#[resource resolver identifier]
> for instance: mssql.query#[root_service].contacts.by_region will translate into this:
> mssql . query # [root_service].contacts (vars interpolation from message) . by_region
> the corresponding sql command should query its own datasource and return contacts list for a specific event.

##### Lets go through the implementation of such resource resolver using mssql and vars interpolation

> javascript 'Destructuring assignment'  and `` 'Template literals' are supported (NodeJS engine)

1. compose an sql query - ```sql select * from table users where user_id = ${user_id}```
2. the guid '[root_service].contacts.by_region' must implement a:
   1. query method 
   2. extractors - for string interpolation (keep in mind that every field from the incoming message can be interpolated into your query)
3. try and catch blocks are encouraged.


> Under nodejs implementation 'Sequoia' can be extended for project specific behavior.
> for remote calls such as (relay from kafka, http request) use exo-sequoia (strapi based resources repository)



## Actors
As of now (1-APR-2020) there are five actors implemented in 'Sequoia' 
1. MsSql
  1.1 Query
  1.2 Stored procedure 
2. Postgres 
  2.1 Query
  2.2 Stored Procedure
  2.3 LTree executor 
3. Static Values 
4. Utils 
5. Global message transformers and augmenters


## Stores
Stores are holding the URI resolvers and instructions.
let's go through store entry implementations.

### MS-Sql Query
```javascript
{
  query : (message) => {
     var {parma1, param2} = message;
    return "select * from [table] where id ='${parma1}' and region='${param2}'";
  }
}
```

### MS-Sql Stored proc
A stored proc named 'sequoia_demo_query'  with a single input param 'country' with value 'italy'

```javascript
{
	query: (message) => {
		const {
			city
		} = message;
		return {
			in: {
				"country": "italy"
			},
			name: "dbo.sequoia_demo_query" //sp name
		}
	}
}
```


### Postgres Query
```javascript
{
  query : (message) => {
     var {parma1, parma2} = message;
    return "select * from [table] where id ='${parma1}' and region='${parma2}'";
  }
}
```


### Static value
```javascript
{
  query : (message) => {
     var {parma1, parma2} = message;
    return "This is a static value of some sort (anything)";
  }
}
```


### Utils value 
> (should you choose to use a third-party lib don't forget to add it to 'Sequoia')
> for instace, yarn add moment --save or npm i moment --save
```javascript
{
  query : (message) => {
     var {} = message;
    return element + country + Datetime.now();
  }
}
```


## Post-Processors and tracers

> Global post-processor enables whole message manipulation.
> only the resolved field value.
> for instance, you have a sql query with ResultSet ([username, country, address])
> you can only manipulate these returned values.

## Post-Processor
A uri/store entry can also implement a post-processors code snippet like so:
```javascript
{
  query: (value,message, currentBranch) => {
     var {id, region} = message;
    return id + country + Datetime.now();
  },
  post: (value) => {
````return value["timestamp"] = Datetime.now();
  }
}
```


## Message Tracer
A uri/store entry can also implement a message tracers for message debugging process in production, code snippet:
```javascript
{
  query: (message) => {
     var {id, country} = message;
    return id + country + Datetime.now();
  },
  trace: (message) => {
    console.dir(message)
  }
}
```

## A demo message

```javascript
/*** Messages */
var demoMessage = {
    "version" : "polar://static.query#relix.events.sequoia.version",
    "about" : "polar://static.query#relix.events.sequoia.about",
    "to": "polar://mssql.query#context.contacts.group",
    "user_id": "123123123123",
    "h" : {
      "j" : "polar://utils.query#common.func.random.guid"
    }
}
```


## Sequoia implementation under node js applications 

```javascript

var sequoia = new sequoiaFactory({
  externalStoresURI : ['http://xxxxxx:1337/sequoias'],
  log: true,
  providers:{ 
    mssql : {
      user: 'uu',
      password: 'pp',
      server: 'xx.xx.xx.xx', 
      database: 'dbname',
    },
    postgres: {
      user : 'postgres',
      dbServer : 'xx.xx.xx.xx',
      database : 'dbname',
      password : 'pp',
      port : 5432
    }
  },
  stores: { /****** extended Stores  *******/
    utils : {                    
        "utils.query#common.info.config.kafka": {
          query: (message) => {
            return `default: ${config.production.kafkaServer}`
          }
        },
      },
      mssql : {
        "mssql.query#context.contacts.region": {
          label: {
            en_US: "",
          },
          query: (message) => {
            var { p1, p2, p3 } = message;
            return `SELECT TOP (1) * FROM df.u = '${p1}'`
          }
        }
      },
      static: {
        "static.query#sequoia.app.author": {
          label: {
            en_US: "",
          },
          query: (message) => {
            return `relix`
          }
        }
      }
    }
  });


//place this call in your streaming messages pipeline 
sequoia.resolve(demoMessage).then(
  result => {
      console.dir(result) 
  },
  error => {
      console.log(error);
  });



//don't forget to free your resources once your application is closing.
//only when you close the application, never during runtime as you would consume resources (pool init and such)
//so, again - DO NOT CLOSE SEQUOIA FOR EVERY MESSAGE JUST WHEN YOUR APP CLOSES  
sequoia.finalize(); 
```


## Client integration

##### a client is any consuming or dipatching application or service(WPF, web, mobile etc.)

