set -ex
# SET THE FOLLOWING VARIABLES
npm i
# local repo username
LOCAL_REPO=valkyrie:8083
# docker hub username
HUB_REPO=smartmil
# image name
IMAGE=sequoia
# ensure we're up to date
# git pull origin master
# bump version
#docker run --rm -v "$PWD":/app treeder/bump patch
version=`cat VERSION`
echo "version: $version"
# run build
./docker-build.sh
# tag it
git add .
git commit -m "version $version"
git push origin master
git tag -a "$version" -m "version $version"
git push --tags
docker tag $LOCAL_REPO/$IMAGE:latest $LOCAL_REPO/$IMAGE:$version
docker tag $HUB_REPO/$IMAGE:latest $HUB_REPO/$IMAGE:$version
# push it
docker push $LOCAL_REPO/$IMAGE:latest
docker push $LOCAL_REPO/$IMAGE:$version

docker push $HUB_REPO/$IMAGE:latest
docker push $HUB_REPO/$IMAGE:$version