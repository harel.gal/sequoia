const sql = require('mssql')

module.exports = function mssql(config) {

    this.config = config;
    this.config.options.enableArithAbort = true;
    this.pool = new sql.ConnectionPool(this.config);
    this.poolConnect = this.pool.connect();

    this.pool.on('error', err => {
        console.error("ERROR: sql provider error, " + err.message);
    })

    

    this.executeQuery = async (query) => {
        await this.poolConnect; 
        const request = this.pool.request();
        const resultSet = await request.query(query)
        var result = Object.assign({},resultSet.recordset);
        return result;
    }

    this.executeProc = async (storedProcDef) => {
        await this.poolConnect; 
        const request = this.pool.request();
        const keys = Object.keys(storedProcDef.in);

        for (idx in keys) {
            const input = keys[idx]
            const value = storedProcDef.in[keys[idx]];
            if(value){
                request.input(input, value);
            }
        }

        try {
            const resultSet = await request.execute(storedProcDef.name);
            var result = Object.assign({}, resultSet.recordset);
            return result;
        }
        catch (e) {
            console.error("ERROR: mssql actor, " + e.message);
        }
        return "";
    }
}