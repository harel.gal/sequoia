const akkaProvider = require("../akka");
const server = require("../server");
const PROTOCOL_PREFIX = "polar://";
const messageIterator = require("../extensions/messageIterator");


/*
Important comment: for firehose processing you need to create a separate process for iteration.
*/
const sequoia = function (config) {

    var $self = this;
    this.initialized = false;
    this.akka_initialized = false;
    this.postProcessorsCollection = [];
    this.akka = {}
    this.serverInitialized = false;


    if (config) {
        this.config = config;
        try {
            this.akka = new akkaProvider(config)
            this.initialized = true;
        }
        catch (e) {
            console.error("ERROR: sequoia AKKA error, " + e.message);
        }
    }

    this.initAkkaStores = async function () {
        try {
            await this.akka.initStores();
        }
        catch (e) {
            console.error("ERROR: error in Akka 'initStores' function" + e);
        }
    }


    this.materializeUri = async function (resourceURI, message, currentBranch, entry) {

        uri = resourceURI.replace(PROTOCOL_PREFIX, "");

        var [pipeline, identifier] = [...uri.split("#")];
        var methods = [...pipeline.split(".")];
        var actor = methods[0];

        var _actor = this.akka.stores.getActor(actor);
        if (_actor == null) {
            return resourceURI + `?unresolved_actor=${actor}`;
        }
        try {
            //execute actor receive
          
            try{
            var { materializedUri, storeEntry } = await _actor.receive.call(_actor,
                { uri, methods: methods.slice(1), identifier, message, currentBranch })

            }
            catch(e){
                console.error("ERROR: actor receive call,  " + e.message)
            }
            try {
                if (storeEntry && storeEntry.post) {
                    this.postProcessorsCollection.push(
                        this.postProcessor(materializedUri, message, storeEntry, currentBranch, entry));
                }
            }
            catch (e) {
                console.error("INFO: postProcessor call, " + e.message)
            }
            return materializedUri;
        }
        catch (e) {
            console.error("ERROR: materializeUri error: " + e.message);
            return uri;
        }
    }

    this.executePortProcessors = async() => {
        this.postProcessorsCollection.forEach(delegate => {
            delegate.call(this);
        });
    }
    this.postProcessor =  (local, global, storeEntry, currentBranch, entry) => { 
        return () => {
            try {
                result = storeEntry.post.call(this, local, global, currentBranch, entry, currentBranch.$$parent);
                return result;
            }
            catch (e) {
                console.error(`ERROR: error executing post processor for , ${uri} - reason: ${e.message}`);
                return local;
            }            
        }
    }

    this.asyncIteration = async function asyncIteration(obj, message, resolver, parent) {
        if (!obj) return;
        
        let entries = Object.keys(obj);

        for (var i = 0; i < entries.length; i++) {
            var entry = entries[i];
            if (typeof obj[entry] == "string") {
                if (obj[entry].includes(PROTOCOL_PREFIX)) {
                    var currentMessageBranch = obj;
                    //messageIterator.addParent(parent);
                    var result = await resolver.call(this, obj[entry], message, obj, entry)
                    if(obj.hasOwnProperty(entry)){
                        obj[entry] = result;
                    }
                }
            } else {
                if (typeof obj[entry] === "object") {
                    await asyncIteration.call(this, obj[entry], message, resolver,obj);
                }
            }
        }
        return message;
    }

    return {
        resolve: async (message) => {

            if (!this.initialized) {
                return message;
            }
            var originalMessage = Object.assign({},message);

            if (!this.akka_initialized) {
                await this.initAkkaStores();
                this.akka_initialized = true;
            }
            message['$$__sequoia'] = true;

            var result = await this.asyncIteration(message, message, 
                    this.materializeUri, 
                    new messageIterator());            

            var messagePrePostProcessors = Object.assign({}, result)
            //message manipulated by ref;
            await this.executePortProcessors();

            if(!this.config.getAllMessagePermutations){
                return result;
            }
            
            return {
                original : originalMessage,
                resolved : messagePrePostProcessors,
                result
            };

        },
        bootstrapServer : (app) => {
            
            if($self.serverInitialized) return;
            $self.serverInitialized = true;
            new server(app)
        },
        getConfig: () => {
            return this.config;
        },
        materializeUri: this.materializeUri,
        finalize: () => {
            //close all open resources (sql & pg connection pool)
            this.akka.stores.freeResources();
        }
    }
}
module.exports = sequoia;