/*var { mssqlStore } = require("../store");
const sql = require('mssql')

module.exports = function mssql(config) {

    var $self = this;
    this.store = mssqlStore;
    this.config = config;
    this.config.options = {}
    this.config.options.enableArithAbort = true;
    this.pool = new sql.ConnectionPool(this.config);
    this.poolConnect = this.pool.connect();

    this.pool.on('error', err => {
        console.error("ERROR: sql provider error, " + err.message);
    })

    this.executeQuery = async (query) => {

        try {
            await this.poolConnect;
            const request = this.pool.request();
            const resultSet = await request.query(query)
            var result = Object.assign({}, resultSet.recordset);
            return result;
        }
        catch (err) {
            console.error('ERROR: SQL error', err);
            return;
        }
    }



    this.executors = {

        query: async (uri, methods, identifier, message, storeNode) => {
            const _query = storeNode.query.call(this, message);
            var resultSet = await this.executeQuery(_query);
            if (!resultSet)
                return _query;
            return resultSet;
        },
        sp: async (uri, methods, identifier, message, storeNode, currentBranch) => {

            await this.poolConnect;
            const request = this.pool.request();
            const storedProcDef = storeNode.sp.call(this, message, currentBranch);

            const keys = Object.keys(storedProcDef.in);
            var idx = 0;
            for (idx = 0; idx < keys.length; idx++) {
                const input = keys[idx]
                const value = storedProcDef.in[keys[idx]];
                request.input(input, value);
            }

            try {
                const resultSet = await request.execute(storedProcDef.name);
                var result = Object.assign({}, resultSet.recordset);

                if (storedProcDef.out) {
                    result = storedProcDef.out.call($self, result);
                }

                return result;
            }
            catch (e) {
                console.error("ERROR: mssql actor, " + e.message);
            }
            return uri + "?unresolved";
        },
        function: (uri, methods, identifier, message, storeNode) => {
            const _query = storeNode.query.call(this, message);
        }
    }

    //todo: refactor this - I don't like it at all. 
    //ad-hoc code, will not scale
    this.out = (resultSet, storedProcDef) => {
        if (!resultSet) return resultSet;

        const { column, delimiter } = storedProcDef;
        var valuesArray = [];

        Object.keys(resultSet).forEach(idx => {
            var value = resultSet[idx][column];
            valuesArray.push(value);
        });
        result = valuesArray.join(delimiter);
        return result;
    }

    return {
        name: "mssql.actor",
        get: () => {
            return $self;
        },
        executeQuery: (query) => {
            return query;
        },
        addStore: (newStore) => {
            Object.keys(newStore).forEach(key => {
                if ($self.store.hasOwnProperty(key)) {
                    var overwrite = newStore[key];
                    console.log(`INFO: overwriting mssql store entry ${key} with values ${JSON.stringify(overwrite)}.`)
                }
                $self.store[key] = newStore[key];
            })
        },
        free: () => {
            console.log("INFO: sql actor connection pool closed.")
            $self.pool.close();
        }
    }
};
*/