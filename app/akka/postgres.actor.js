var {postgresStore} = require("../store");
const { Pool, Client } = require('pg')


module.exports = function postgres(config){
    
    var $self = this;
    this.store = postgresStore;
    this.config = config;

    this.pool = new Pool({
        user: this.config.user,
        host: this.config.dbServer,
        database: this.config.database,
        password: this.config.password,
        port: this.config.port
      })

    this.pool.on('error', (err, client) => {
        console.error('ERROR: postgres connection pool, Unexpected error on idle client', err)
    })

    
    this.executeQuery = async(query) => {
      
      var result = '';
      try {
        const client = await this.pool.connect();
        try {
            const res = await client.query(query)
            result = Object.assign({},res.rows);
          } 
          catch(e){
              console.error("ERROR: postgres query error, " + e.message);
          }
          finally {
            client.release()
          }
          return result;
      } 
      catch (err) {
        console.error('ERROR: PG SQL error', err);
        return;
      }
    }


   
   this.executors = {

       query : async(uri, methods, identifier, message,storeNode) => {
          const _query = storeNode.query.call(this, message);
          var resultSet =  await this.executeQuery(_query);        
          return resultSet;
       },
       ltree : async(uri, methods, identifier, message,storeNode) => {
         return `INFO NOT IMPLEMENTED: ltree processor is not implemented for ${uri}`;
            //not implemented
        },
       sp : (uri, methods, identifier, message,storeNode) => { //stored proc
          return "INFO NOT IMPLEMENTED: stored proc processor is not implemented"
       },
       function : (uri, methods, identifier, message,storeNode) => {
        return "INFO NOT IMPLEMENTED: function processor is not implemented"
       }
   }
   
   return {
    name : "postgres.actor",
    get : () => {
        return $self;
    },
    addStore : (newStore) => {
      Object.keys(newStore).forEach(key => {
        if($self.store.hasOwnProperty(key)){
          var overwrite = newStore[key];
          console.log(`INFO: overwriting mssql store entry ${key} with values ${JSON.stringify(overwrite)}.`)
        }
        $self.store[key] = newStore[key];
      })
    },
    /*
    receive : async function(dataload){
      var {uri, methods, identifier, message} = dataload;
      var storeEntry = $self.store[uri];
      if(storeEntry==null){
        console.error(`INFO: no store entry defined for uri, ${uri}`);
        return uri;
      }
      var result = await $self.executors[methods[0]].call($self,uri, methods.slice(1), identifier, message, storeEntry);
            
      if(!result){
        return uri + "?unresolved"; 
      }
      return {materializedUri : result, storeEntry: storeEntry || {}};
    },
    */
    free : () =>{
      $self.pool.end().then(() => console.log("INFO: PG sql actor connection pool closed."))
    }
   }
 };