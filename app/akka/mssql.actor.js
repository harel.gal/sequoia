var { mssqlStore } = require("../store");

module.exports = function mssql(config) {

    var $self = this;
    this.store = mssqlStore;

    this.executeQuery = async (query) => {
        var t = "";
    }


    this.executors = {

        query: async (uri, methods, identifier, message, storeNode) => {
            const _query = storeNode.query.call(this, message);
            var provider = this.getProvider('mssql')
            var result = await provider.execq(_query);
            if (!result)
                return _query;
            return result;
        },
        sp: async (uri, methods, identifier, message, storeNode, currentBranch) => {

            //await this.poolConnect;
            //const request = this.pool.request();
            const storedProcDef = storeNode.sp.call(this, message, currentBranch);
            
            
            try {
                
                var provider = this.getProvider('mssql')
                var result = await provider.executeProc(storedProcDef);

                if (storedProcDef.out) {
                    result = storedProcDef.out.call($self, result);
                }

                return result;
            }
            catch (e) {
                console.error("ERROR: mssql actor, " + e.message);
            }
            return uri + "?unresolved";
        },
        function: (uri, methods, identifier, message, storeNode) => {
            const _query = storeNode.query.call(this, message);
        }
    }

    //todo: comment to Chana, please refactor this - ad-hoc code, will not scale.
    this.out = (resultSet, storedProcDef) => {
        if (!resultSet) return resultSet;

        const { column, delimiter } = storedProcDef;
        var valuesArray = [];

        Object.keys(resultSet).forEach(idx => {
            var value = resultSet[idx][column];
            valuesArray.push(value);
        });
        result = valuesArray.join(delimiter);
        return result;
    }

    return {
        name: "mssql.actor",
        get: () => {
            return $self;
        },
        executeQuery: (query) => {
            return query;
        },
        addStore: (newStore) => {
            Object.keys(newStore).forEach(key => {
                if ($self.store.hasOwnProperty(key)) {
                    var overwrite = newStore[key];
                    console.log(`INFO: overwriting mssql store entry ${key} with values ${JSON.stringify(overwrite)}.`)
                }
                $self.store[key] = newStore[key];
            })
        },
        free: () => {
            console.log("INFO: sql actor connection pool closed.")
            $self.pool.close();
        }
    }
};