const mssqlActor = require("./mssql.actor");
const staticActor = require("./static.actor");
const utilsActor = require("./utils.actor");
const postgresActor = require("./postgres.actor");
const globalActor = require("./global.actor");
const { externalStoresLoader } = require('../store');
const { mssqlProvider } = require('../providers');
const ActorsFactory = [mssqlActor, staticActor, utilsActor, postgresActor, globalActor];

const BuiltInStores = function (config) {
    this.config = config || {};

    this.providersFactory = {
        "mssql": (config) => {
            return new mssqlProvider(config)
        },
        "kafka": () => {

        },
        "redis": () => {

        }
    }

    this.actorsFactory = {
        "mssql": (config) => {
            return new mssqlActor(config);
        },
        "postgres": (config) => {
            return new postgresActor(config);
        },
        "static": (config) => {
            return new staticActor(config);
        },
        "utils": (config) => {
            return new utilsActor(config);
        },
        "global": (config) => {
            return new globalActor(config);
        }
    };

    this.actors = {};
    this.providers = {};

    this.init = () => {

        ////providers config 
        let that = this;

        var providersConfigSection = this.config.providers;
        if (!providersConfigSection) {
            console.error("ERROR: no providers were defined. MSSQL, PG etc.");
            return this;
        }

        //init Providers 
        Object.keys(this.providersFactory).forEach(provider => {

            var config = providersConfigSection.hasOwnProperty(provider) ? providersConfigSection[provider] : {};
            this.providersFactory[provider] = this.providersFactory[provider].call(this, config);

            try {

            }
            catch (e) {

            }
        });


        Object.keys(this.actorsFactory).forEach(actor => {
            //providers -> config.providers sent to Sequoia
            var config = providersConfigSection.hasOwnProperty(actor) ? providersConfigSection[actor] : {}

            try {
                this.actors[actor] = this.actorsFactory[actor].call(this, config);

                //get providers delegate

                this.actors[actor].get().getProvider = (providerName) => {
                    if (this.providersFactory.hasOwnProperty(providerName)) {
                        return this.providersFactory[providerName];

                    }
                    console.error(`ERROR: no provider was found for ${providerName}`)
                }

                this.actors[actor].receive = receive;

            }
            catch (e) {
                console.error("ERROR: Akka Init, " + e.message);
            };
        });
        return this;
    }

    this.loadExternalStores = async function () {
        const extStoresLoader = new externalStoresLoader(this.config);
        return await extStoresLoader.load()
    }

    this.populateStores = async (stores, storeSource) => {
        if (!stores) return;

        Object.keys(stores).forEach(exStore => {
            if (this.actors.hasOwnProperty(exStore)) {
                if (this.actors[exStore].hasOwnProperty('addStore')) {
                    var actor = this.actors[exStore];
                    Object.keys(stores[exStore]).forEach(entry => {
                        stores[exStore][entry].source = storeSource;
                        actor.addStore({ [entry]: stores[exStore][entry] });
                        console.log(`DEBUG: Store source: ${storeSource} <- Store entry: ${entry}`);
                    });

                } else {
                    console.log("INFO: can not extend this store," + exStore)
                }
            }
        });
    }

    this.extend = async () => {
        var { stores } = this.config;
        if (!stores) return this;

        Object.keys(this.actors).forEach(key => {
            var actor = this.actors[key];
            let store = actor.get().store;
            Object.keys(store).forEach(x => {
                x.source = "internal";
                console.log(`DEBUG: Store source: Sequoia internal store <- Store entry: ${x}`);
            })
            //console.log(`DEBUG: Store source: ${storeSource} <- Store entry: ${entry}`);
        })

        try {
            const remote_stores = await this.loadExternalStores();
            await this.populateStores(remote_stores, "remote");
        }
        catch (e) {
            console.error("ERROR: failed to populate stores from 'exo'" + e);
        }

        try {
            await this.populateStores(stores, "application");
        }
        catch (e) {
            console.error("ERROR: failed to populate stores from 'local'" + e);
        }
        return this;
    }

    this.getActor = (name) => {
        if (this.actors.hasOwnProperty(name)) {
            return this.actors[name];
        }
        return null;
    }

    this.freeResources = () => {
        Object.keys(this.actors).forEach(actor => {
            try {
                this.actors[actor].free();
            }

            catch (e) {
                console.log("ERROR:" + e.message)
            }
        })
    }
}

module.exports = function AkkaProvider(config) {

    this.initStores = async () => {
        this.stores = await new BuiltInStores(config).init().extend();
    }

}


//Global receive function
var receive = async function receive(dataload) {
    var { uri, methods, identifier, message, currentBranch } = dataload;
    var store = this.get().store;
    var storeEntry = this.get().store[uri];
    var result = "";
    if (storeEntry == null) {
        console.error(`INFO: no store entry defined for uri, ${uri}`);
        result = uri + "?unresolved";
    } else {
        //console.log(`DEBUG: entry resolved from ${storeEntry.source} store entry`)
        result = await this.get().executors[methods[0]].call(this.get(), uri, methods.slice(1), identifier, message, storeEntry, currentBranch);
        if (!result) {
            result = uri + "?unresolved";
        }
    }
    return { materializedUri: result, storeEntry: storeEntry || {} };
}

