
const { globalStore } = require("../store");

module.exports = function (config) {

    var $self = this;
    this.config = config;
    this.store = globalStore;

    this.executors = {
        pattern: () => {

        },
        query: async (uri, methods, identifier, message, storeNode) => {
            return await storeNode.query.call(this, message, this.getProvider);
        },
        resolve: (uri, methods, identifier, message, storeNode) => {
            return storeNode.query.call(this, message);
        }
    }

    return {
        name: "global.actor",
        get: () => {
            return $self;
        },
        addStore: (newStore) => {
            Object.keys(newStore).forEach(key => {
                if ($self.store.hasOwnProperty(key)) {
                    console.log(`INFO: overwriting mssql store entry ${key} with values ${console.dir(newStore[key])}.`)
                }
                $self.store[key] = newStore[key];
            })
        },
        receive: async (dataload) => {
            var { uri, methods, identifier, message } = dataload;
            var storeEntry = $self.store[uri];
            if (storeEntry == null) {
                console.error(`INFO: no store entry defined for uri, ${uri}`);
                return uri;
            }

            var result = await $self.executors[methods[0]].call($self, uri, methods.slice(1), identifier, message, storeEntry);
            return { materializedUri: uri + "?done", storeEntry: storeEntry || {} };

        },

        free: () => {

        }
    }
};