var sequoiaFactory = require('../sequoia')
//var sequoia = new sequoiaFactory();

var sequoia = new sequoiaFactory({
    externalStoresURI: [],
    log: true,
    getAllMessagePermutations: false,
    providers: {
        mssql: {
            "user": "DevServiceUser",
            "domain": "managix",
            "password": "ysTe2eapqW6PVS0fuqZ0",
            "server": "madrid.managix.local",
            "database": "smng.data",
            "options": {
                "useUTC": false
            },
            "pool": {
                "max": 100,
                "min": 5,
                "idleTimeoutMillis": 30000
            }
        },
        postgres: {
            user: 'postgres',
            dbServer: 'pg.service.v.valerian',
            database: 'MauiDB',
            password: 'password',
            port: 5432
        }
    },
    stores: { /****** extended Stores  *******/
        utils: {
            "utils.query#common.info.config.kafka": {
                query: (message) => {
                    return `action-items configuration file ${config.production.kafkaServer}`
                }
            },
            "utils.query#common.func.number": {
                query: (message) => {
                    return "number"
                }
            }
        },
        mssql: {
            "mssql.query#context.contacts.region": {
                label: {
                    he_IL: "",
                },
                query: (message) => {
                    var { EventElementId, EventTemplateId, DeviceId } = message;
                    return `SELECT TOP (1) * FROM [SMNG.DATA].[dbo].[User]`
                }
            },
            "mssql.query#context.contacts.group": {
                label: {
                    he_IL: "",
                },
                query: (message) => {
                    var { EventElementId, EventTemplateId, DeviceId } = message;
                    return `SELECT TOP (10) * FROM [SMNG.DATA].[dbo].[User]`
                }
            },
            "mssql.sp#voya.contacts.by_region":
            {
                sp: (message, currentBranch) => {
                    const EventElementId = message.instruction.EventElementId;
                    //const MessageToList = currentBranch.MessageToList

                    return {
                        in: {
                            "EventElementId": EventElementId
                        },
                        name: "dbo.Usp_GetContactsToEvent",
                        out: (resultSet) => {

                            var column = 'id';
                            var delimiter = ';';
                            var valuesArray = [];

                            Object.keys(resultSet).forEach(idx => {
                                var value = resultSet[idx][column];
                                valuesArray.push(value);
                            });
                            result = valuesArray.join(delimiter);
                            return result;
                        }
                    }
                }, post: (newValue, message, currentBranch, entry) => {
                    var a = "";
                }
            }
        },
        static: {
            "static.query#sequoia.app.author": {
                label: {
                    he_IL: "",
                },
                query: (message) => {
                    return `relix`
                }
            }
        }
    }
});

//local vs global
var testMessage = {
    "mssql.query": "polar://mssql.query#local.demo.query",
    "mssql.stored": "polar://mssql.sp#sequoia.examples.sp",
    "postgres.ltree": "polar://postgres.ltree#local.demo.ltree",
    "postgres.stored": "polar://postgres.sp#local.demo.stored.proc",
    "postgres.query": "polar://postgres.query#local.demo.query",
    "static.version": "polar://static.query#local.sequoia.version",
    "static.about": "polar://static.query#local.sequoia.about",
    "utils.timestamp": "polar://utils.query#common.func.timestamp",
    "utils.guid": "polar://utils.query#common.func.random.guid"
}



var demoMessage = {
    "instructions": {
        "replace_all": "polar://global.query#translate.all.guids"
    },
    "author": "polar://static.query#sequoia.author", //sync
    "version": "polar://static.query#relix.events.sequoia.version", //sync
    "about": "polar://static.query#relix.events.sequoia.about", //sync 
    "to": "polar://mssql.query#context.contacts.group", //async
    "user_id": "86f56643-65a1-4108-9dd3-d391b4575969",
    "operation": "Update.Status",
    "op": {
        "operation": "Update.Status"
    },
    "from": "polar://mssql.query#context.contacts.country",
    "history": "polar://postgres.query#events.history.limit10",
    "EventTemplateId": "a41e1442-8e39-4c66-a5c3-b817b795e74c",
    "DeviceId": "00000000-0000-0000-0000-000000000000",
    "event": "polar://static.query#relix.events.high_radiation_levels",
    "e": "polar://static.query#relix.events.co2_anomaly",
    "a": "polar://utils.query#common.func.number",
    "b": "polar://utils.query#common.func.timestamp",
    "array": ["polar://utils.query#common.func.timestamp", "polar://utils.query#common.func.random.guid"],
    "c": "polar://utils.query#common.events.demo",
    "d": "polar://utils.query#common.func.timestamp",
    "f": "polar://utils.query#common.func.random.guid",
    "h": {
        "j": "polar://utils.query#common.func.random.guid"
    }
}


//"replace_all" : "polar://global.query#translate.all.guids",
var a = {
    "tobase64": "polar://global.query#convert.toBase64",
    "replace_all": "polar://global.query#translate.all.guids",
    "user_id": "86f56643-65a1-4108-9dd3-d391b4575969",
    "EventTemplateId": "a41e1442-8e39-4c66-a5c3-b817b795e74c",
    "from": "polar://mssql.query#context.contacts.region",
    "history": "polar://postgres.query#events.history.limit10",
    "EventTemplateId": "a41e1442-8e39-4c66-a5c3-b817b795e74c",
    "DeviceId": "00000000-0000-0000-0000-000000000000",
    "event": "polar://static.query#relix.events.high_radiation_levels",
    "e": "polar://static.query#relix.events.co2_anomaly",
    "a": "polar://utils.query#common.func.number",
    "b": "polar://utils.query#common.func.timestamp",
    "array": ["polar://utils.query#common.func.timestamp", "polar://utils.query#common.func.random.guid"],
    "c": "polar://utils.query#common.events.demo",
    "d": "polar://utils.query#common.func.timestamp",
    "f": "polar://utils.query#common.func.random.guid",
    "h": {
        "j": "polar://utils.query#common.func.random.guid"
    }
}

var a1 = {
    "_id": "5ed886d13ceb588e60cb13f8",
    "Name": "תרחיש מלא",
    "FreeText": null,
    "SkeletonArtifactId": "5eafb2e660d3890010ad892a",
    "EventTemplateId": "01fcf660-8803-46c6-bb72-b6d804b2eeff",
    "DeviceId": "00000000-0000-0000-0000-000000000000",
    "Steps": [
        {
            "Name": "אתר מטלה",
            "BuildingBlockArtifactId": "5db9394269c5cc01f638e7ad",
            "Order": 0,
            "Url": "https://www.google.com/",
            "FreeText": null,
            "SubCategory": "None",
            "IsAutoExecute": true,
            "SkeletonStepId": null,
            "Category": "Unspecified",
            "_id": 1
        },
        {
            "Name": "High CO values command",
            "BuildingBlockArtifactId": "5e78de61f378c700107bf07c",
            "Order": "1",
            "Url": null,
            "FreeText": null,
            "SubCategory": "None",
            "IsAutoExecute": "true",
            "SkeletonStepId": null,
            "Category": "DaCommand",
            "_id": "2",
            "Description": null,
            "DraftStepId": null,
            "State": "InProgress",
            "UpdatingMachineName": "ZAGREB",
            "UpdateTimeStamp": "2020-06-04T08:29:53.4006312+03:00",
            "ExecutionDateTime": "2020-06-04T08:29:53.4006312+03:00"
        },
        {
            "Name": "טופס פרטי רכב",
            "BuildingBlockArtifactId": "5dd16d9645181901f39d88ad",
            "Order": 2,
            "Url": null,
            "FreeText": null,
            "SubCategory": "None",
            "IsAutoExecute": false,
            "SkeletonStepId": null,
            "Category": "Form",
            "_id": 3
        },
        {
            "Name": "אשר אירוע",
            "BuildingBlockArtifactId": "5cffb15fc88b344dabdad68e",
            "Order": 3,
            "Url": null,
            "FreeText": null,
            "SubCategory": "None",
            "IsAutoExecute": true,
            "SkeletonStepId": null,
            "Category": "Command",
            "_id": 4
        },
        {
            "Name": "High CO values",
            "BuildingBlockArtifactId": "5e78dd81f378c700107bf07a",
            "Order": 4,
            "Url": null,
            "FreeText": null,
            "SubCategory": "Message",
            "IsAutoExecute": false,
            "SkeletonStepId": null,
            "Category": "DaCommand",
            "_id": 5
        },
        {
            "Name": "מטלת כריזה 1",
            "BuildingBlockArtifactId": "5e37f45e581fc0001021cbf1",
            "Order": "5",
            "Url": null,
            "FreeText": null,
            "SubCategory": "Announcement",
            "IsAutoExecute": "true",
            "SkeletonStepId": null,
            "Category": "DaCommand",
            "_id": "6",
            "Description": null,
            "DraftStepId": null,
            "State": "InProgress",
            "UpdatingMachineName": "Vienna",
            "UpdateTimeStamp": "2020-06-04T08:29:53.4120366+03:00",
            "ExecutionDateTime": "2020-06-04T08:29:53.4120366+03:00"
        },
        {
            "Name": "idan activity",
            "BuildingBlockArtifactId": "5ea17454e746fc00109487e2",
            "Order": 6,
            "Url": null,
            "FreeText": null,
            "SubCategory": "CreateActivity",
            "IsAutoExecute": true,
            "SkeletonStepId": null,
            "Category": "Command"
        }],
    "created": "2020-06-04T05:29:53.635Z",
    "EventElementId": "ef351477-7593-495e-8517-5d7f6ae84b4f",
    "contactid": "4C307D6D-8DCD-4F9B-8624-60235B539E83",
    "sequoia": "polar://global.query#translate.all.guids"
}

var a2 = {
    "instruction":
    {
        "EventElementId": "35bc37a1-e9fc-48af-b8fe-cd027e669041"
    },
    "step": {
        "Name": "Message command 4",
        "BuildingBlockArtifactId": "5f05a0e86692e80011634880",
        "Order": 0,
        "Url": null,
        "FreeText": null,
        "SubCategory": "Message",
        "IsAutoExecute": false,
        "SkeletonStepId": null,
        "Category": "DaCommand",
        "DaParameters": {
            "da": "Voya",
            "Command": "DistributionMessage",
            "DistributionType": "108e01eb-d0ed-41e9-98bd-639b61d89b04",
            "MessageToList": "012d2d21-d82c-433e-a649-08687e4aadbe;f99b86f5-d1df-4005-97c8-10de055cda4a;f99b86f5-d1df-4005-97c8-10de055cda4a;",
            "instruct": ["polar://mssql.sp#voya.contacts.by_region"]
        },
        "_id": 1,
        "instructions": {
            "correlationId": "5f0aeb9e74503058a4db4227",
            "jobId": "5f0aeb9e74503058a4db4228",
            "stepId": 1,
            "state": "NotStarted",
            "DistributionType": "108e01eb-d0ed-41e9-98bd-639b61d89b04",
            "MessageToList": ["012d2d21-d82c-433e-a649-08687e4aadbe", "f99b86f5-d1df-4005-97c8-10de055cda4a", "polar://mssql.sp#voya.contacts.by_region", "polar://mssql.sp#voya.contacts.by_region"]
        }
    }
}
a2.sequoia = "polar://global.query#translate.all.guids";      
sequoia.bootstrapServer({});


sequoia.resolve(a2).then(
    result => {
        console.log(JSON.stringify(result));
    },
    error => {
        console.log(error);
    });
