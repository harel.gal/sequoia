
//express js middleware / handler
class ServerExtension {
    constructor(app) {
        this.app = app;
    }

    bootstrap(store) {
        this.app.get('/api/sequoia/sequoias',
            (req, res) => {
                res.setHeader('Content-Type', 'application/json');
                res.status(200).type('json').send({});
            });
    }

}

//use: in your express bootstrap page import/require ServerExtension and call
//new ServerExtension(app) //pass the express app object and that's it
module.exports = ServerExtension;

