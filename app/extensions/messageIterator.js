module.exports = function messageIterator() {
    this.index = {};

    this.setValue = (key, newValue) => {
        //need to add check for key is present in collection
        if (!key) return; 
        key = key.toLowerCase();
        if(!this.index[key]){
            console.log("INFO: key not found in pointers collection, flattree indexer.");
        }
        this.index[key](newValue);
    };

    this.setIndex = (key, func) => {
        if(!key) return;
        key = key.toLowerCase();
        this.index[key] = func;
    };

    this.getParent = () => {
        
    }

    this.setPointers = (key, obj, entry) => {
        key = key.toLowerCase();
        if(!this.index[key]) return;
        this.index[key] = this.index[key](obj, entry);
    };

    this.getAllKeys = () => {
        return "'" + Object.keys(this.index).join("','") + "'";
    };
};

