
 
    const RecursiveConditionalIndexer = function RecursiveConditionalIndexer(obj, condition, flatTree, branch){
    
      if(!obj) return;
      let entries = Object.keys(obj);
    
      for(var i=0;i<entries.length;i++){
        var entry = entries[i];
        var value = obj[entry];
        if (typeof value != "object") {
          if(condition(value)){
            flatTree.setIndex(value, (o,e) => {
              return (newValue) => {
                o[e] = newValue;
              }
            });
            
            flatTree.setPointers(value, obj, entry);
          }
        } else {
          RecursiveConditionalIndexer.call(this, obj[entry], condition, flatTree, branch + "." + entry)
        }
      }
    return flatTree;
   }
  
  module.exports ={RecursiveConditionalIndexer}



