var mssqlStore = require("./mssql");
var staticStore = require("./static");
var utilsStore = require("./utils");
var postgresStore = require("./postgres");
var globalStore = require("./global")
var externalStoresLoader = require("./externalStoresLoader");

module.exports = {
    mssqlStore, staticStore, utilsStore, postgresStore, externalStoresLoader,globalStore
}