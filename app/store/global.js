const extensions=require('../extensions');
const flatTreeIndexer = require('../extensions/flatTreeIndexer');
var Base64 = require('js-base64').Base64;
module.exports = {
    "global.query#convert.toBase64" : {
        query : async (message, getProvider) => {
            try{                
                message["base64"] = Base64.encode(JSON.stringify(message));
            }
            catch(e){
                console.error("ERROR: base 64 conversion failed." + e.message)
            }
        }
    },
    "global.query#translate.all.guids" : {

        query : async (message, getProvider) => {
        
            var flatTree = new flatTreeIndexer();
  
            extensions.RecursiveConditionalIndexer(message, value => {
                let re = new RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i);
                return re.test(value);
            }, flatTree, "root");
         
            var guid_collection =flatTree.getAllKeys() 
            if(guid_collection.length > 0){
                var resultSet=[];
                var sql= `SELECT LOWER(convert(nvarchar(50), [Key])) as [key], Value as value FROM [SMNG.DATA].[dbo].[vw_UidDictionary] WHERE [Key] IN (${guid_collection})`
                var provider = getProvider('mssql');
                var resultSet =  await provider.executeQuery(sql); 
                for(idx in resultSet){   
                    var f = resultSet[idx];                              
                    flatTree.setValue(f.key, f.value);
                };       
            }
        }
    }
  }


  
/*
module.exports = {
    "global.query#translate.all.guids": {
        pattern : (value) => {
            let re = new RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i);
            return re.test(value);
        },        
        query: () => {

        }
    },
  }*/
  
  