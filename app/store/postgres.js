module.exports = {
    "postgres.query#local.demo.query": {
      query: () => {
        return `SELECT version();`
      }
    },
    "postgres.sp#local.demo.stored.proc": {
      query: () => {
        return `SHOW server_version;`
      }
    },
    "postgres.ltree#local.demo.ltree": {
      query: () => {
        return `SELECT version();`
      }
    }
  }



  /*
  ltree alias query example
  "Select max(revision) as revision, path, rr.resource_id as resource_id, value, resource_type " +
  "FROM resources_registry as rr " +
  "LEFT JOIN resources_materialized as rm " +
  "ON rr.resource_id=rm.resource_id " +
  "where rr.path ~ (select resource_lquery as path from resources_aliases where resource_alias = '%s') " +
  "group by path, rr.resource_id,rm.value, rr.resource_type " +
  "order by rr.resource_id, revision desc";
  */