const axios = require('axios');


const externalStoresLoader = function (config) {

    this.config = config;
    this.actors = {};

    this.load = async function () {

        const { externalStoresURI } = this.config;

        return new Promise(async(resolve, reject) => {
            //relix 31/03/2020 - Corona day number 15
            //todo: right!!! like anyone is going to look at this code
            //add support for multiple stores
            for(index in externalStoresURI){
                await this.loadAndRestructure(externalStoresURI[index])
            };
            resolve(this.actors);
        });
    }

    this.loadAndRestructure = async (storeURI) => {
        try{
            //var response = await axios.create({baseURL : storeURI, method : 'get', timeout: 500});
            var response = await axios.get(storeURI);
            var data = response.data;
            if(!data) return {};
            data.forEach(element => {
                if (element.resolver != null){
                    //clear editor values \n 
                    element.resolver = element.resolver.replace(/[\n]*/g, "");
                    this.appendResourceResolverToStore(element);
                }
            });
        }
        catch(e){
            console.error("ERROR: external stores loader, " + e.message);
            //do nothing
        }
    }

    this.appendResourceResolverToStore = (resource) => {
        if(!this.actors.hasOwnProperty(resource.actor)){
            this.actors[resource.actor] = {}
        }
        try{
            var resolver = resource.resolver;
            resource[resource.uri.split(".")[1].split("#")[0]] = eval(resource.resolver);
            if(resource.post_processor && resource.post_processor_active){
                try{
                    resource["post"] = eval(resource.post_processor);
                }
                catch(e){
                    console.error("ERROR: external stores post processor, reason:" + e.message)
                }
            }
            this.actors[resource.actor][resource.uri] = resource;
        }
        catch(e){
            console.error("ERROR: external stores appendResourceResolverToStore, reason:" + e.message)
        }
    }
    
    /*
    1. web forms 
    2. virtualization resources 
    3. storage with templates to RV
    */
    /*
    [{
	"id": 1,
	"label": "Relix",
	"created_at": "2020-03-31T07:13:19.409Z",
	"updated_at": "2020-03-31T07:20:12.355Z",
	"uri": "static.query#globals.sequoia.author",
	"identifier": "globals.sequoia.author",
	"actor": "static",
	"resolver": "{\nquery : () => {\nreturn \"Harel Gal\"\n}\n}",
	"documentation": "{\n  query : () => {\n    return \"Harel Gal\"\n  }\n}",
	"branches": [{
		"id": 1,
		"label": "action items branch",
		"identifier": "action-items",
		"comments": "this is a generic action items branch",
		"created_at": "2020-03-31T08:00:38.282Z",
		"updated_at": "2020-03-31T08:05:14.269Z",
		"versions": null,
		"sequoia": 1
	}]
}]
    */

}
module.exports = externalStoresLoader;
