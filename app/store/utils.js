var moment = require('moment');

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}

  

module.exports = {
        "utils.query#common.func.timestamp": {
            query: () => {
                return  moment().format();
            }
        },
        "utils.query#common.func.random.guid": {
            query: () => {
                return uuidv4();
            }
        }
    }
