//in the future add externals as a docker map to extensions files 
module.exports = {
        "static.query#local.sequoia.version" : {
            query : (message) => {
                return "Sequoia 2020 Version 1.0";                            
            }
        },
        "static.query#local.sequoia.about" : {
            query : (message) => {
                return "Sequoia is a uri based messages augmentation service.";
            }
        }    
    }

