module.exports = {
  "mssql.query#local.demo.query": {
    query: () => {
      return `SELECT @@VERSION`
    },
    post: (result) => {
      result["__relix"] = "store post processor";
      return result;
    }
  },
  "mssql.sp#local.demo.stored.proc": {
    query: () => {
      return `SELECT @@VERSION`
    }
  }
}
