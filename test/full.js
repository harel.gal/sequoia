var expect = require("chai").expect;
var sequoiaFactory = require('../app/sequoia')


var sequoia = new sequoiaFactory({
    externalStoresURI: [],
    log: true,
    getAllMessagePermutations: false,
    providers: {
        mssql: {
            "user": "DevServiceUser",
            "domain": "managix",
            "password": "ysTe2eapqW6PVS0fuqZ0",
            "server": "madrid.managix.local",
            "database": "smng.data",
            "options": {
                "useUTC": false
            },
            "pool": {
                "max": 100,
                "min": 5,
                "idleTimeoutMillis": 30000
            }
        },
        postgres: {
            user: 'postgres',
            dbServer: 'pg.service.v.valerian',
            database: 'MauiDB',
            password: 'password',
            port: 5432
        }
    },
    stores: { /****** extended Stores  *******/
        utils: {
            "utils.query#common.info.config.kafka": {
                query: (message) => {
                    return `action-items configuration file ${config.production.kafkaServer}`
                }
            },
            "utils.query#common.func.number": {
                query: (message) => {
                    return "number"
                }
            }
        },
        mssql: {
            "mssql.query#context.contacts.region": {
                label: {
                    he_IL: "",
                },
                query: (message) => {
                    var { param1, param2, param3 } = message;
                    return `SELECT TOP (1) * FROM [xxxx].[xxxx].[xxxxx] where param1 = ${param1}`
                }
            },
            "mssql.sp#voya.contacts.by_region":
            {
                sp: (message, currentBranch) => {
                    const param = message.instruction.param;

                    return {
                        in: {
                            "param": param
                        },
                        name: "dbo.usp_xxxx",
                        out: (resultSet) => {
                            return result;
                        }
                    }
                }, post: (newValue, message, currentBranch, entry) => {
                    var a = "";
                }
            }
        },
        static: {
            "static.query#sequoia.app.author": {
                label: {
                    he_IL: "",
                },
                query: (message) => {
                    return `relix`
                }
            }
        }
    }
});


describe("AKKA: test various streaming messages ", function () {
    describe("All actors augmentation test", function () {
        it("resolves all fields", (done) => {

            var tinyMessage = {
                "author" : "polar://static.query#sequoia.app.author"
            }

            sequoia.bootstrapServer({});
            sequoia.resolve(tinyMessage).then(
                result => {
                    var resolved = result;
                    expect(resolved.author).equal('relix');
                    done();
                },
                error => {
                    console.log(error);
                    done({ error : error});
                });
        });
    });
});